

Current database is ForestryDB log in with TIMEBARS2 dog4$p..1
Final sql for prod relese in early 2021 re in folder FinalSQL2020Redo


# Loading data

/home/jcox/Sites/Formal/tbordswebservice/DataFiles/CSVFromTB/tbTimebars.csv

Must create the Timebars table first, then the look up tables res and md

Must import look up tables first, Resources and Metadata, then last do Timebars table




# Timebars SQL Schemas
```
This Repo describes the requirements for the new tbOrdsAPI.
The Outcome of this REST API will be the enablement of publishing data 
to an Oracle database from Timebars front end client.

The following are the table Names on Client Side that must map to the REST API
for create, read, update and delete operations (CRUD)


tbAdminPanel
bTimebars
tbResources
tbTags
tbMetaData
tbBaseline
tbResCalcs
tbDocuments

TBADMINPANEL
TBBASELINE
TBDOCUMENTS
TBMETADATA
TBRESCALCS
TBRESOURCES
TBTAGS
TBTIMEBARS

Each table will have the following standard set of fields

"CREATED" TIMESTAMP(6),
"CREATED_BY" VARCHAR2(255 BYTE),
"UPDATED" TIMESTAMP(6),
"UPDATED_BY" VARCHAR2(255 BYTE) 

```

# Timebars

```
All good

Must be created before tbMetaData
but Populated after

```


# tbMetadata
created cascalde delet foreign key on TBMDID with TBTIMEBARS TBID
Removed the FK for now, see sql on right to add back in

make sure the following are numbers
	
	"TBMDOPPORTUNITYCOST" NUMBER(13,4), 
	"TBMDSUNKCOSTS" NUMBER(13,4),	
	"TBMDPAYBACKPERIOD" NUMBER, 
    "TBMDNETPRESENTVALUE" NUMBER, 
	"TBMDBENEFITCOSTRATIO" NUMBER, 
    "TBMDINTERNALRATEOFRETURN" NUMBER, 
	"TBMDTBMDECNOMICVALUEADDED" NUMBER, 
    "TBMDPAYBACKPERIOD" NUMBER, 
    "TBMDSORTORDER" NUMBER, 
 	"TBMDWEIGHTING" NUMBER, 
    "CANVASNO" NUMBER, 
	tbmdLastModified




# Tags
need to remove first row from csv


# Baseline

```
use duplicate of tbTimebars table, 



```


# AdminPanel

```
all good


```


# Resources

```
all good

```


# ResCalcs

```
all fixed up, no errors

```

# Documents

watch sort order and html content
