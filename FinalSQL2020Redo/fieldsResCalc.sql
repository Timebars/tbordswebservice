
/* rescalc get */

select "id", "tbResCalcID", "tbResCalcCustomerID", "tbResCalcAzureID", "tbResCalcName", "tbResCalcTbID", "tbResCalcWdays", "tbResCalcStartWeekNo", 
"tbResCalcTbWeekNo", "tbResCalcTbResID", "tbResCalcWeekSumHours", "tbResCalcWeekSumCost", "tbResCalcWeek", "tbResCalcHours", 
"tbResCalcLastModified", "tbResCalcMonth", "tbResCalcVolume", "tbResCalcWrkMonths", "tbResCalcStatusDateStartMonthNo", "tbResCalcTbStartMonthNo", 
"tbResCalcTbStartYearNo", "tbResCalcMonthSumVolume", "tbResCalcMonthSumVolPine", "tbResCalcMonthSumVolFir", "tbResCalcMonthSumVolBalsam", 
"tbResCalcMonthSumVolSpruce","tbResCalcStage","tbResCalcState","tbResCalcStatus", "tbResCalcGroup1", "tbResCalcGroup2", "tbResCalcGroup3", "tbResCalcL1", "tbResCalcL2", "tbResCalcL3", "tbResCalcL4", 
"tbResCalcL5","tbResCalcWeek1", "tbResCalcWeek2", "tbResCalcWeek3", "tbResCalcWeek4", "tbResCalcWeek5", "tbResCalcWeek6", "tbResCalcWeek7", 
"tbResCalcWeek8", "tbResCalcWeek9", "tbResCalcWeek10", "tbResCalcWeek11", "tbResCalcWeek12", "tbResCalcWeek13", "tbResCalcWeek14", "tbResCalcWeek15", 
"tbResCalcWeek16", "tbResCalcWeek17", "tbResCalcWeek18", "tbResCalcWeek19", "tbResCalcWeek20", "tbResCalcWeek21", "tbResCalcWeek22", 
"tbResCalcWeek23", "tbResCalcWeek24", "tbResCalcWeek25", "tbResCalcWeek26", "tbResCalcWeek27", "tbResCalcWeek28", "tbResCalcWeek29", "tbResCalcWeek30"
from tbrescalcs

//,"tbResCalcStage","tbResCalcState","tbResCalcStatus"

//"tbResCalcStage"=:tbrescalcstage,"tbResCalcState"=:tbrescalcstate,"tbResCalcStatus"=:tbrescalcstatus

/* rescalc post */

begin
insert into tbrescalcs(
"tbResCalcID", "tbResCalcCustomerID", "tbResCalcAzureID", "tbResCalcName", "tbResCalcTbID", "tbResCalcWdays", "tbResCalcStartWeekNo", 
"tbResCalcTbWeekNo", "tbResCalcTbResID", "tbResCalcWeekSumHours", "tbResCalcWeekSumCost", "tbResCalcWeek", "tbResCalcHours", 
"tbResCalcLastModified", "tbResCalcMonth", "tbResCalcVolume", "tbResCalcWrkMonths", "tbResCalcStatusDateStartMonthNo", "tbResCalcTbStartMonthNo", 
"tbResCalcTbStartYearNo", "tbResCalcMonthSumVolume", "tbResCalcMonthSumVolPine", "tbResCalcMonthSumVolFir", "tbResCalcMonthSumVolBalsam", 
"tbResCalcMonthSumVolSpruce","tbResCalcStage","tbResCalcState","tbResCalcStatus", "tbResCalcGroup1", "tbResCalcGroup2", "tbResCalcGroup3", "tbResCalcL1", "tbResCalcL2", "tbResCalcL3", "tbResCalcL4", 
"tbResCalcL5","tbResCalcWeek1", "tbResCalcWeek2", "tbResCalcWeek3", "tbResCalcWeek4", "tbResCalcWeek5", "tbResCalcWeek6", "tbResCalcWeek7", 
"tbResCalcWeek8", "tbResCalcWeek9", "tbResCalcWeek10", "tbResCalcWeek11", "tbResCalcWeek12", "tbResCalcWeek13", "tbResCalcWeek14", "tbResCalcWeek15", 
"tbResCalcWeek16", "tbResCalcWeek17", "tbResCalcWeek18", "tbResCalcWeek19", "tbResCalcWeek20", "tbResCalcWeek21", "tbResCalcWeek22", 
"tbResCalcWeek23", "tbResCalcWeek24", "tbResCalcWeek25", "tbResCalcWeek26", "tbResCalcWeek27", "tbResCalcWeek28", "tbResCalcWeek29", "tbResCalcWeek30"
) 
values (
:tbrescalcid, :tbrescalccustomerid, :tbrescalcazureid, :tbrescalcname, :tbrescalctbid, :tbrescalcwdays, :tbrescalcstartweekno, :tbrescalctbweekno, 
:tbrescalctbresid, :tbrescalcweeksumhours, :tbrescalcweeksumcost, :tbrescalcweek, :tbrescalchours, :tbrescalclastmodified, :tbrescalcmonth, 
:tbrescalcvolume, :tbrescalcwrkmonths, :tbrescalcstatusdatestartmonthno, :tbrescalctbstartmonthno, :tbrescalctbstartyearno, :tbrescalcmonthsumvolume, 
:tbrescalcmonthsumvolpine, :tbrescalcmonthsumvolfir, :tbrescalcmonthsumvolbalsam, :tbrescalcmonthsumvolspruce,
:tbrescalcstage, :tbrescalcstate, :tbrescalcstatus, :tbrescalcgroup1, :tbrescalcgroup2, 
:tbrescalcgroup3, :tbrescalcl1, :tbrescalcl2, :tbrescalcl3, :tbrescalcl4, :tbrescalcl5, :tbrescalcweek1, :tbrescalcweek2, :tbrescalcweek3, 
:tbrescalcweek4, :tbrescalcweek5, :tbrescalcweek6, :tbrescalcweek7, :tbrescalcweek8, :tbrescalcweek9, :tbrescalcweek10, :tbrescalcweek11, 
:tbrescalcweek12, :tbrescalcweek13, :tbrescalcweek14, :tbrescalcweek15, :tbrescalcweek16, :tbrescalcweek17, :tbrescalcweek18, :tbrescalcweek19, 
:tbrescalcweek20, :tbrescalcweek21, :tbrescalcweek22, :tbrescalcweek23, :tbrescalcweek24, :tbrescalcweek25, :tbrescalcweek26, :tbrescalcweek27, 
:tbrescalcweek28, :tbrescalcweek29, :tbrescalcweek30
);
end;


/* rescalc put */
begin
update tbrescalcs set 
"tbResCalcID"=:tbrescalcid,"tbResCalcCustomerID"=:tbrescalccustomerid,"tbResCalcAzureID"=:tbrescalcazureid,"tbResCalcName"=:tbrescalcname,
"tbResCalcTbID"=:tbrescalctbid,"tbResCalcWdays"=:tbrescalcwdays,"tbResCalcStartWeekNo"=:tbrescalcstartweekno,"tbResCalcTbWeekNo"=:tbrescalctbweekno,
"tbResCalcTbResID"=:tbrescalctbresid,"tbResCalcWeekSumHours"=:tbrescalcweeksumhours,"tbResCalcWeekSumCost"=:tbrescalcweeksumcost,
"tbResCalcWeek"=:tbrescalcweek,"tbResCalcHours"=:tbrescalchours,"tbResCalcLastModified"=:tbrescalclastmodified,"tbResCalcMonth"=:tbrescalcmonth,
"tbResCalcVolume"=:tbrescalcvolume,"tbResCalcWrkMonths"=:tbrescalcwrkmonths,"tbResCalcStatusDateStartMonthNo"=:tbrescalcstatusdatestartmonthno,
"tbResCalcTbStartMonthNo"=:tbrescalctbstartmonthno,"tbResCalcTbStartYearNo"=:tbrescalctbstartyearno,
"tbResCalcMonthSumVolume"=:tbrescalcmonthsumvolume,"tbResCalcMonthSumVolPine"=:tbrescalcmonthsumvolpine,
"tbResCalcMonthSumVolFir"=:tbrescalcmonthsumvolfir,"tbResCalcMonthSumVolBalsam"=:tbrescalcmonthsumvolbalsam,
"tbResCalcMonthSumVolSpruce"=:tbrescalcmonthsumvolspruce,
"tbResCalcStage"=:tbrescalcstage,"tbResCalcState"=:tbrescalcstate,"tbResCalcStatus"=:tbrescalcstatus,
"tbResCalcGroup1"=:tbrescalcgroup1,"tbResCalcGroup2"=:tbrescalcgroup2,
"tbResCalcGroup3"=:tbrescalcgroup3,"tbResCalcL1"=:tbrescalcl1,"tbResCalcL2"=:tbrescalcl2,"tbResCalcL3"=:tbrescalcl3,"tbResCalcL4"=:tbrescalcl4,
"tbResCalcL5"=:tbrescalcl5,"tbResCalcWeek1"=:tbrescalcweek1,"tbResCalcWeek2"=:tbrescalcweek2,"tbResCalcWeek3"=:tbrescalcweek3,
"tbResCalcWeek4"=:tbrescalcweek4,"tbResCalcWeek5"=:tbrescalcweek5,"tbResCalcWeek6"=:tbrescalcweek6,"tbResCalcWeek7"=:tbrescalcweek7,
"tbResCalcWeek8"=:tbrescalcweek8,"tbResCalcWeek9"=:tbrescalcweek9,"tbResCalcWeek10"=:tbrescalcweek10,"tbResCalcWeek11"=:tbrescalcweek11,
"tbResCalcWeek12"=:tbrescalcweek12,"tbResCalcWeek13"=:tbrescalcweek13,"tbResCalcWeek14"=:tbrescalcweek14,"tbResCalcWeek15"=:tbrescalcweek15,
"tbResCalcWeek16"=:tbrescalcweek16,"tbResCalcWeek17"=:tbrescalcweek17,"tbResCalcWeek18"=:tbrescalcweek18,"tbResCalcWeek19"=:tbrescalcweek19,
"tbResCalcWeek20"=:tbrescalcweek20,"tbResCalcWeek21"=:tbrescalcweek21,"tbResCalcWeek22"=:tbrescalcweek22,"tbResCalcWeek23"=:tbrescalcweek23,
"tbResCalcWeek24"=:tbrescalcweek24,"tbResCalcWeek25"=:tbrescalcweek25,"tbResCalcWeek26"=:tbrescalcweek26,"tbResCalcWeek27"=:tbrescalcweek27,
"tbResCalcWeek28"=:tbrescalcweek28,"tbResCalcWeek29"=:tbrescalcweek29,"tbResCalcWeek30"=:tbrescalcweek30 
where "tbrescalcid"=:id;
end;


/* final sql */


  CREATE TABLE "TIMEBARS2"."TBRESCALCS" 
   (	"id" NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  NOT NULL ENABLE, 
	"tbResCalcID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP" NOT NULL ENABLE, 
	"tbResCalcName" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcTbResID" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcTbID" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcWeek" DATE, 
	"tbResCalcHours" NUMBER, 
	"tbResCalcWdays" NUMBER, 
	"tbResCalcStartWeekNo" NUMBER, 
	"tbResCalcTbWeekNo" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcMonth" DATE, 
	"tbResCalcVolume" NUMBER, 
	"tbResCalcWrkMonths" NUMBER, 
	"tbResCalcStatusDateStartMonthNo" NUMBER, 
	"tbResCalcTbStartMonthNo" NUMBER, 
	"tbResCalcTbStartYearNo" NUMBER, 
	"tbResCalcWeekSumHours" NUMBER, 
	"tbResCalcWeekSumCost" NUMBER, 
	"tbResCalcMonthSumVolume" NUMBER, 
	"tbResCalcMonthSumVolPine" NUMBER, 
	"tbResCalcMonthSumVolFir" NUMBER, 
	"tbResCalcMonthSumVolBalsam" NUMBER, 
	"tbResCalcMonthSumVolSpruce" NUMBER, 
    
    "tbResCalcStage" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcState" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcStatus" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
        
	"tbResCalcGroup1" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcGroup2" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcGroup3" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcL1" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcL2" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcL3" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcL4" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcL5" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcWeek1" NUMBER, 
	"tbResCalcWeek2" NUMBER, 
	"tbResCalcWeek3" NUMBER, 
	"tbResCalcWeek4" NUMBER, 
	"tbResCalcWeek5" NUMBER, 
	"tbResCalcWeek6" NUMBER, 
	"tbResCalcWeek7" NUMBER, 
	"tbResCalcWeek8" NUMBER, 
	"tbResCalcWeek9" NUMBER, 
	"tbResCalcWeek10" NUMBER, 
	"tbResCalcWeek11" NUMBER, 
	"tbResCalcWeek12" NUMBER, 
	"tbResCalcWeek13" NUMBER, 
	"tbResCalcWeek14" NUMBER, 
	"tbResCalcWeek15" NUMBER, 
	"tbResCalcWeek16" NUMBER, 
	"tbResCalcWeek17" NUMBER, 
	"tbResCalcWeek18" NUMBER, 
	"tbResCalcWeek19" NUMBER, 
	"tbResCalcWeek20" NUMBER, 
	"tbResCalcWeek21" NUMBER, 
	"tbResCalcWeek22" NUMBER, 
	"tbResCalcWeek23" NUMBER, 
	"tbResCalcWeek24" NUMBER, 
	"tbResCalcWeek25" NUMBER, 
	"tbResCalcWeek26" NUMBER, 
	"tbResCalcWeek27" NUMBER, 
	"tbResCalcWeek28" NUMBER, 
	"tbResCalcWeek29" NUMBER, 
	"tbResCalcWeek30" NUMBER, 
	"tbResCalcAzureID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResCalcCustomerID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
    "tbResCalcLastModified" DATE,
	"CREATED" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"UPDATED" TIMESTAMP (6), 
	"UPDATED_BY" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP"
	
   )  DEFAULT COLLATION "USING_NLS_COMP" SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 10 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "DATA" ;
  CREATE UNIQUE INDEX "TIMEBARS2"."TBRESCALCS_CON" ON "TIMEBARS2"."TBRESCALCS" ("tbResCalcID") 
  PCTFREE 10 INITRANS 20 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "DATA" ;
ALTER TABLE "TIMEBARS2"."TBRESCALCS" ADD CONSTRAINT "TBRESCALCS_CON" PRIMARY KEY ("tbResCalcID")
  USING INDEX "TIMEBARS2"."TBRESCALCS_CON"  ENABLE;

  CREATE OR REPLACE EDITIONABLE TRIGGER "TIMEBARS2"."BIU_TBRESCALCS" 
    before insert or update on TBRESCALCS
    for each row
begin

    if inserting then
        :new.created    := localtimestamp;
        :new.created_by := nvl(wwv_flow.g_user,user);
    end if;
    :new.updated    := localtimestamp;
    :new.updated_by := nvl(wwv_flow.g_user,user);
end;
/
ALTER TRIGGER "TIMEBARS2"."BIU_TBRESCALCS" ENABLE;
