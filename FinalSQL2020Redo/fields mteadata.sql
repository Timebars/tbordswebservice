
/* rescalc get */

"tbMDID","tbMDName","tbMDProjectType","tbMDPortfolio","tbMDProgram","tbMDExecutiveSummary","tbMDGate","tbMDDeliveryManager","tbMDBusinessAdvisor",
"tbMDBusinessOwner","tbMDEstimationClass","tbMDROMEstimate","tbMDInvestmentCategory","tbMDInvestmentInitiative","tbMDInvestmentObjective","tbMDInvestmentStrategy",
"tbMDPriorityStrategic","tbMDCustomerID","tbMDAzureID","tbMDNameShort","tbMDProjectNumber","tbMDDescription","tbMDNotes","tbMDExtLink1","tbMDExtSystemID1",
"tbMDSortOrder","tbMDtbLastModified","tbMDPriority","tbMDStatus","tbMDState","tbMDSeverity","tbMDStage","tbMDPhase","tbMDCategory","tbMDHealth","tbMDResponsibility",
"tbMDDepartment","tbMDExSponsor","tbMDPM","tbMDShowIn","tbMDYesNoSelector","tbMDProduct","tbMDContact","tbMDWBS","tbMDWeighting","tbMDLocation","tbMDPrimarySkill",
"tbMDPrimaryRole","tbMDOther2","tbMDOther3","tbMDOther4","canvasNo","tbMDProgActivityAlignment","tbMDBackgroundInfo","tbMDConsequence","tbMDExpectedBenfits",
"tbMDProblemOpportunity","tbMDCapabilitiesNeeded","tbMDSeniorLevelCommittment","tbMDSprintName","tbMDStakeholderDescription","tbMDHealthCost","tbMDHealthHours",
"tbMDHealthIssues","tbMDHealthOverall","tbMDHealthRisk","tbMDHealthSchedule","tbMDHealthScope","tbMDBenefitCostRatio","tbMDConstraintsAssumptions",
"tbMDContractNumber","tbMDCostBenefitAnalysis","tbMDEcnomicValueAdded","tbMDInternalRateOfReturn","tbMDNetPresentValue","tbMDOpportunityCost","tbMDOrgManager",
"tbMDPaybackPeriod","tbMDRiskVsSizeAndComplexity","tbMDSize","tbMDStageApprover","tbMDSunkCosts","tbMDWrittenBy","tbMDPrimaryLineOfBusiness","tbMDNotesWorkflow",
"tbMDSyncNotes","tbMDSponsoringDepartment","tbMDPrimaryContact","tbMDContactNumber","tbMDResponsibleTeam","tbMDNotesProject","tbMDOther5","tbMDOther6","tbMDOther7",
"tbMDOther8","tbMDOther9","tbMDOther10","tbMDOther11","tbMDOther12","tbMDRefID"


/* rescalc post */

:tbmdid, :tbmdname, :tbmdprojecttype, :tbmdportfolio, :tbmdprogram, :tbmdexecutivesummary, :tbmdgate, :tbmddeliverymanager, :tbmdbusinessadvisor, :tbmdbusinessowner, :tbmdestimationclass, :tbmdromestimate, :tbmdinvestmentcategory, :tbmdinvestmentinitiative, :tbmdinvestmentobjective, :tbmdinvestmentstrategy, :tbmdprioritystrategic, :tbmdcustomerid, :tbmdazureid, :tbmdnameshort, :tbmdprojectnumber, :tbmddescription, :tbmdnotes, :tbmdextlink1, :tbmdextsystemid1, :tbmdsortorder, :tbmdtblastmodified, :tbmdpriority, :tbmdstatus, :tbmdstate, :tbmdseverity, :tbmdstage, :tbmdphase, :tbmdcategory, :tbmdhealth, :tbmdresponsibility, :tbmddepartment, :tbmdexsponsor, :tbmdpm, :tbmdshowin, :tbmdyesnoselector, :tbmdproduct, :tbmdcontact, :tbmdwbs, :tbmdweighting, :tbmdlocation, :tbmdprimaryskill, :tbmdprimaryrole, :tbmdother2, :tbmdother3, :tbmdother4, :canvasno, :tbmdprogactivityalignment, :tbmdbackgroundinfo, :tbmdconsequence, :tbmdexpectedbenfits, :tbmdproblemopportunity, :tbmdcapabilitiesneeded, :tbmdseniorlevelcommittment, :tbmdsprintname, :tbmdstakeholderdescription, :tbmdhealthcost, :tbmdhealthhours, :tbmdhealthissues, :tbmdhealthoverall, :tbmdhealthrisk, :tbmdhealthschedule, :tbmdhealthscope, :tbmdbenefitcostratio, :tbmdconstraintsassumptions, :tbmdcontractnumber, :tbmdcostbenefitanalysis, :tbmdecnomicvalueadded, :tbmdinternalrateofreturn, :tbmdnetpresentvalue, :tbmdopportunitycost, :tbmdorgmanager, :tbmdpaybackperiod, :tbmdriskvssizeandcomplexity, :tbmdsize, :tbmdstageapprover, :tbmdsunkcosts, :tbmdwrittenby, :tbmdprimarylineofbusiness, :tbmdnotesworkflow, :tbmdsyncnotes, :tbmdsponsoringdepartment, :tbmdprimarycontact, :tbmdcontactnumber, :tbmdresponsibleteam, :tbmdnotesproject, :tbmdother5, :tbmdother6, :tbmdother7, :tbmdother8, :tbmdother9, :tbmdother10, :tbmdother11, :tbmdother12, :tbmdrefid


/* rescalc put */
"tbMDID"=:tbmdid,"tbMDName"=:tbmdname,"tbMDProjectType"=:tbmdprojecttype,"tbMDPortfolio"=:tbmdportfolio,"tbMDProgram"=:tbmdprogram,"tbMDExecutiveSummary"=:tbmdexecutivesummary,"tbMDGate"=:tbmdgate,"tbMDDeliveryManager"=:tbmddeliverymanager,"tbMDBusinessAdvisor"=:tbmdbusinessadvisor,"tbMDBusinessOwner"=:tbmdbusinessowner,"tbMDEstimationClass"=:tbmdestimationclass,"tbMDROMEstimate"=:tbmdromestimate,"tbMDInvestmentCategory"=:tbmdinvestmentcategory,"tbMDInvestmentInitiative"=:tbmdinvestmentinitiative,"tbMDInvestmentObjective"=:tbmdinvestmentobjective,"tbMDInvestmentStrategy"=:tbmdinvestmentstrategy,"tbMDPriorityStrategic"=:tbmdprioritystrategic,"tbMDCustomerID"=:tbmdcustomerid,"tbMDAzureID"=:tbmdazureid,"tbMDNameShort"=:tbmdnameshort,"tbMDProjectNumber"=:tbmdprojectnumber,"tbMDDescription"=:tbmddescription,"tbMDNotes"=:tbmdnotes,"tbMDExtLink1"=:tbmdextlink1,"tbMDExtSystemID1"=:tbmdextsystemid1,"tbMDSortOrder"=:tbmdsortorder,"tbMDtbLastModified"=:tbmdtblastmodified,"tbMDPriority"=:tbmdpriority,"tbMDStatus"=:tbmdstatus,"tbMDState"=:tbmdstate,"tbMDSeverity"=:tbmdseverity,"tbMDStage"=:tbmdstage,"tbMDPhase"=:tbmdphase,"tbMDCategory"=:tbmdcategory,"tbMDHealth"=:tbmdhealth,"tbMDResponsibility"=:tbmdresponsibility,"tbMDDepartment"=:tbmddepartment,"tbMDExSponsor"=:tbmdexsponsor,"tbMDPM"=:tbmdpm,"tbMDShowIn"=:tbmdshowin,"tbMDYesNoSelector"=:tbmdyesnoselector,"tbMDProduct"=:tbmdproduct,"tbMDContact"=:tbmdcontact,"tbMDWBS"=:tbmdwbs,"tbMDWeighting"=:tbmdweighting,"tbMDLocation"=:tbmdlocation,"tbMDPrimarySkill"=:tbmdprimaryskill,"tbMDPrimaryRole"=:tbmdprimaryrole,"tbMDOther2"=:tbmdother2,"tbMDOther3"=:tbmdother3,"tbMDOther4"=:tbmdother4,"canvasNo"=:canvasno,"tbMDProgActivityAlignment"=:tbmdprogactivityalignment,"tbMDBackgroundInfo"=:tbmdbackgroundinfo,"tbMDConsequence"=:tbmdconsequence,"tbMDExpectedBenfits"=:tbmdexpectedbenfits,"tbMDProblemOpportunity"=:tbmdproblemopportunity,"tbMDCapabilitiesNeeded"=:tbmdcapabilitiesneeded,"tbMDSeniorLevelCommittment"=:tbmdseniorlevelcommittment,"tbMDSprintName"=:tbmdsprintname,"tbMDStakeholderDescription"=:tbmdstakeholderdescription,"tbMDHealthCost"=:tbmdhealthcost,"tbMDHealthHours"=:tbmdhealthhours,"tbMDHealthIssues"=:tbmdhealthissues,"tbMDHealthOverall"=:tbmdhealthoverall,"tbMDHealthRisk"=:tbmdhealthrisk,"tbMDHealthSchedule"=:tbmdhealthschedule,"tbMDHealthScope"=:tbmdhealthscope,"tbMDBenefitCostRatio"=:tbmdbenefitcostratio,"tbMDConstraintsAssumptions"=:tbmdconstraintsassumptions,"tbMDContractNumber"=:tbmdcontractnumber,"tbMDCostBenefitAnalysis"=:tbmdcostbenefitanalysis,"tbMDEcnomicValueAdded"=:tbmdecnomicvalueadded,"tbMDInternalRateOfReturn"=:tbmdinternalrateofreturn,"tbMDNetPresentValue"=:tbmdnetpresentvalue,"tbMDOpportunityCost"=:tbmdopportunitycost,"tbMDOrgManager"=:tbmdorgmanager,"tbMDPaybackPeriod"=:tbmdpaybackperiod,"tbMDRiskVsSizeAndComplexity"=:tbmdriskvssizeandcomplexity,"tbMDSize"=:tbmdsize,"tbMDStageApprover"=:tbmdstageapprover,"tbMDSunkCosts"=:tbmdsunkcosts,"tbMDWrittenBy"=:tbmdwrittenby,"tbMDPrimaryLineOfBusiness"=:tbmdprimarylineofbusiness,"tbMDNotesWorkflow"=:tbmdnotesworkflow,"tbMDSyncNotes"=:tbmdsyncnotes,"tbMDSponsoringDepartment"=:tbmdsponsoringdepartment,"tbMDPrimaryContact"=:tbmdprimarycontact,"tbMDContactNumber"=:tbmdcontactnumber,"tbMDResponsibleTeam"=:tbmdresponsibleteam,"tbMDNotesProject"=:tbmdnotesproject,"tbMDOther5"=:tbmdother5,"tbMDOther6"=:tbmdother6,"tbMDOther7"=:tbmdother7,"tbMDOther8"=:tbmdother8,"tbMDOther9"=:tbmdother9,"tbMDOther10"=:tbmdother10,"tbMDOther11"=:tbmdother11,"tbMDOther12"=:tbmdother12,"tbMDRefID"=:tbmdrefid,


/* final sql */


  CREATE TABLE "TIMEBARS2"."TBMETADATA" 
   (	"ID" NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  NOT NULL ENABLE, 
	"tbMDID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPM" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDWBS" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"canvasNo" NUMBER, 
	"tbMDGate" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDName" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDSize" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDNotes" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPhase" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDRefID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDStage" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDState" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDHealth" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther2" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther3" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther4" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther5" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther6" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther7" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther8" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther9" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDShowIn" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDStatus" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDAzureID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDContact" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther10" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther11" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOther12" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDProduct" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDProgram" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDCategory" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDExtLink1" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDLocation" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPriority" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDSeverity" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDExSponsor" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDNameShort" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPortfolio" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDSortOrder" NUMBER, 
	"tbMDSunkCosts" NUMBER(13,4), 
	"tbMDSyncNotes" VARCHAR2(5000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDWeighting" NUMBER, 
	"tbMDWrittenBy" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDCustomerID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDDepartment" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDHealthCost" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDHealthRisk" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDOrgManager" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDSprintName" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDConsequence" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDDescription" VARCHAR2(8000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDHealthHours" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDHealthScope" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPrimaryRole" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDProjectType" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDROMEstimate" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDExtSystemID1" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDHealthIssues" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDNotesProject" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPrimarySkill" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDBusinessOwner" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDHealthOverall" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDNotesWorkflow" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPaybackPeriod" NUMBER, 
	"tbMDProjectNumber" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDStageApprover" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDYesNoSelector" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDBackgroundInfo" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDHealthSchedule" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPrimaryContact" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDResponsibility" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDtbLastModified" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDBusinessAdvisor" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDDeliveryManager" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDEstimationClass" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDExpectedBenfits" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDNetPresentValue" NUMBER, 
	"tbMDOpportunityCost" NUMBER(13,4), 
	"tbMDResponsibleTeam" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDBenefitCostRatio" NUMBER, 
	"tbMDExecutiveSummary" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPriorityStrategic" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDContactNumber" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDCapabilitiesNeeded" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDInvestmentCategory" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDInvestmentStrategy" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDProblemOpportunity" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDContractNumber" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDCostBenefitAnalysis" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDInvestmentObjective" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDInternalRateOfReturn" NUMBER, 
	"tbMDInvestmentInitiative" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDSponsoringDepartment" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDPrimaryLineOfBusiness" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDProgActivityAlignment" VARCHAR2(1000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDEcnomicValueAdded" NUMBER, 
	"tbMDConstraintsAssumptions" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDSeniorLevelCommittment" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDStakeholderDescription" VARCHAR2(4000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMDRiskVsSizeAndComplexity" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"CREATED" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"UPDATED" TIMESTAMP (6), 
	"UPDATED_BY" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP"
   )  DEFAULT COLLATION "USING_NLS_COMP" SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 10 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA" ;

  CREATE OR REPLACE EDITIONABLE TRIGGER "TIMEBARS2"."BIU_TBMETADATA" 
    before insert or update on TBMETADATA
    for each row
begin

    if inserting then
        :new.created    := localtimestamp;
        :new.created_by := nvl(wwv_flow.g_user,user);
    end if;
    :new.updated    := localtimestamp;
    :new.updated_by := nvl(wwv_flow.g_user,user);
end;

/
ALTER TRIGGER "TIMEBARS2"."BIU_TBMETADATA" ENABLE;





// fields from excel
    const fieldsArr = [
        "tbMDID",
        "tbMDName",
        "tbMDProjectType",
        "tbMDPortfolio",
        "tbMDProgram",
        "tbMDExecutiveSummary",
        "tbMDGate",
        "tbMDDeliveryManager",
        "tbMDBusinessAdvisor",
        "tbMDBusinessOwner",
        "tbMDEstimationClass",
        "tbMDROMEstimate",
        "tbMDInvestmentCategory",
        "tbMDInvestmentInitiative",
        "tbMDInvestmentObjective",
        "tbMDInvestmentStrategy",
        "tbMDPriorityStrategic",
        "tbMDCustomerID",
        "tbMDAzureID",
        "tbMDNameShort",
        "tbMDProjectNumber",
        "tbMDDescription",
        "tbMDNotes",
        "tbMDExtLink1",
        "tbMDExtSystemID1",
        "tbMDSortOrder",
        "tbMDtbLastModified",
        "tbMDPriority",
        "tbMDStatus",
        "tbMDState",
        "tbMDSeverity",
        "tbMDStage",
        "tbMDPhase",
        "tbMDCategory",
        "tbMDHealth",
        "tbMDResponsibility",
        "tbMDDepartment",
        "tbMDExSponsor",
        "tbMDPM",
        "tbMDShowIn",
        "tbMDYesNoSelector",
        "tbMDProduct",
        "tbMDContact",
        "tbMDWBS",
        "tbMDWeighting",
        "tbMDLocation",
        "tbMDPrimarySkill",
        "tbMDPrimaryRole",
        "tbMDOther2",
        "tbMDOther3",
        "tbMDOther4",
        "canvasNo",
        "tbMDProgActivityAlignment",
        "tbMDBackgroundInfo",
        "tbMDConsequence",
        "tbMDExpectedBenfits",
        "tbMDProblemOpportunity",
        "tbMDCapabilitiesNeeded",
        "tbMDSeniorLevelCommittment",
        "tbMDSprintName",
        "tbMDStakeholderDescription",
        "tbMDHealthCost",
        "tbMDHealthHours",
        "tbMDHealthIssues",
        "tbMDHealthOverall",
        "tbMDHealthRisk",
        "tbMDHealthSchedule",
        "tbMDHealthScope",
        "tbMDBenefitCostRatio",
        "tbMDConstraintsAssumptions",
        "tbMDContractNumber",
        "tbMDCostBenefitAnalysis",
        "tbMDEcnomicValueAdded",
        "tbMDInternalRateOfReturn",
        "tbMDNetPresentValue",
        "tbMDOpportunityCost",
        "tbMDOrgManager",
        "tbMDPaybackPeriod",
        "tbMDRiskVsSizeAndComplexity",
        "tbMDSize",
        "tbMDStageApprover",
        "tbMDSunkCosts",
        "tbMDWrittenBy",
        "tbMDPrimaryLineOfBusiness",
        "tbMDNotesWorkflow",
        "tbMDSyncNotes",
        "tbMDSponsoringDepartment",
        "tbMDPrimaryContact",
        "tbMDContactNumber",
        "tbMDResponsibleTeam",
        "tbMDNotesProject",
        "tbMDOther5",
        "tbMDOther6",
        "tbMDOther7",
        "tbMDOther8",
        "tbMDOther9",
        "tbMDOther10",
        "tbMDOther11",
        "tbMDOther12",
        "tbMDRefID"
    ]

