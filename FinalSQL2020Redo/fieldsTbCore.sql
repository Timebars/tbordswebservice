
/* tvCore get */
select 
"tbID","tbSelfKey2","tbCoordTop","tbCoordLeft","tbHierarchyOrder","tbName","tbType","tbL1","tbL2",
"tbL3","tbL4","tbL5","tbOwner","tbStart","tbFinish","tbDuration","tbAStart","tbAFinish","tbResID",
"tbCalendar","tbPercentTimeOn","tbWork","tbAWork","tbWorkRemaining",
"tbPercentComplete","tbCost","tbAcost","tbCostRemaining","tbPayRate","tbCostType","tbCostID",
"tbConstraintType","tbConstraintDate","tbFloat","tbFreeFloat","tbExpHoursPerWeek","canvasNo",
"tbCustomerID","tbMetaDataID","tbBLID","tbPredecessor","tbAzureID","tbBarColor",
"tbTextColor","kbCoordLeft","kbCoordTop","focdItemCoordLeft","focdItemCoordTop","tbCountry","tbCompany",
"tbLanguage","tbPriority","tbStatus","tbStep","tbStepStatus","tbState","tbWfReasonNote","tbStage",
"tbPhase","tbPine","tbBalsam","tbFir","tbSpruce","tbArea","tbSize","tbPercent2","tbVolume","tbT1",
"tbT2","tbT3","tbT4","tbT5","tbTaskParent","tbRemainingDuration"
from TBTIMEBARS

/* removed from above */
"tbRemainingDuration",

/* tvCore post */"tbConstraintType","tbConstraintDate",

begin
insert into TBTIMEBARS("tbID","tbSelfKey2","tbCoordTop","tbCoordLeft","tbHierarchyOrder","tbName","tbType","tbL1","tbL2",
"tbL3","tbL4","tbL5","tbOwner","tbStart","tbFinish","tbDuration","tbAStart","tbAFinish","tbResID",
"tbCalendar","tbPercentTimeOn","tbWork","tbAWork","tbWorkRemaining",
"tbPercentComplete","tbCost","tbAcost","tbCostRemaining","tbPayRate","tbCostType","tbCostID",
"tbConstraintType","tbConstraintDate","tbFloat","tbFreeFloat","tbExpHoursPerWeek","canvasNo",
"tbCustomerID","tbMetaDataID","tbBLID","tbPredecessor","tbAzureID","tbBarColor",
"tbTextColor","kbCoordLeft","kbCoordTop","focdItemCoordLeft","focdItemCoordTop","tbCountry","tbCompany",
"tbLanguage","tbPriority","tbStatus","tbStep","tbStepStatus","tbState","tbWfReasonNote","tbStage",
"tbPhase","tbPine","tbBalsam","tbFir","tbSpruce","tbArea","tbSize","tbPercent2","tbVolume","tbT1",
"tbT2","tbT3","tbT4","tbT5","tbTaskParent","tbRemainingDuration")
values (
:tbid, :tbselfkey2, :tbcoordtop, :tbcoordleft, :tbhierarchyorder, :tbname, :tbtype, :tbl1, :tbl2, 
:tbl3, :tbl4, :tbl5, :tbowner, :tbstart, :tbfinish, :tbduration, :tbastart, :tbafinish, 
:tbresid, :tbcalendar, :tbpercenttimeon, :tbwork, :tbawork, :tbworkremaining, 
:tbpercentcomplete, :tbcost, :tbacost, :tbcostremaining, :tbpayrate, :tbcosttype, :tbcostid, 
:tbconstrainttype, :tbconstraintdate, :tbfloat, :tbfreefloat, :tbexphoursperweek, :canvasno, :tbcustomerid, 
:tbmetadataid, :tbblid, :tbpredecessor, :tbazureid, :tbbarcolor, 
:tbtextcolor, :kbcoordleft, :kbcoordtop, :focditemcoordleft, :focditemcoordtop, :tbcountry,
:tbcompany, :tblanguage, :tbpriority, :tbstatus, :tbstep, :tbstepstatus, :tbstate, :tbwfreasonnote, 
:tbstage, :tbphase, :tbpine, :tbbalsam, :tbfir, :tbspruce, :tbarea, :tbsize, :tbpercent2, :tbvolume, 
:tbt1, :tbt2, :tbt3, :tbt4, :tbt5, :tbtaskparent, :tbremainingduration
);
end;


/* tvCore put */

begin
update TBTIMEBARS set 
"tbID"=:tbid,"tbSelfKey2"=:tbselfkey2,"tbCoordTop"=:tbcoordtop,"tbCoordLeft"=:tbcoordleft,"tbHierarchyOrder"=:tbhierarchyorder,
"tbName"=:tbname,"tbType"=:tbtype,"tbL1"=:tbl1,"tbL2"=:tbl2,"tbL3"=:tbl3,"tbL4"=:tbl4,"tbL5"=:tbl5,"tbOwner"=:tbowner,"tbStart"=:tbstart,
"tbFinish"=:tbfinish,"tbDuration"=:tbduration,"tbAStart"=:tbastart,"tbAFinish"=:tbafinish,"tbRemainingDuration"=:tbremainingduration,
"tbResID"=:tbresid,"tbCalendar"=:tbcalendar,"tbPercentTimeOn"=:tbpercenttimeon,"tbWork"=:tbwork,"tbAWork"=:tbawork,
"tbWorkRemaining"=:tbworkremaining,"tbPercentComplete"=:tbpercentcomplete,"tbCost"=:tbcost,"tbAcost"=:tbacost,
"tbCostRemaining"=:tbcostremaining,"tbPayRate"=:tbpayrate,"tbCostType"=:tbcosttype,"tbCostID"=:tbcostid,
"tbConstraintType"=:tbconstrainttype,"tbConstraintDate"=:tbconstraintdate,"tbFloat"=:tbfloat,"tbFreeFloat"=:tbfreefloat,
"tbExpHoursPerWeek"=:tbexphoursperweek,"canvasNo"=:canvasno,"tbCustomerID"=:tbcustomerid,"tbMetaDataID"=:tbmetadataid,
"tbBLID"=:tbblid,"tbPredecessor"=:tbpredecessor,"tbAzureID"=:tbazureid,"tbBarColor"=:tbbarcolor,
"tbTextColor"=:tbtextcolor,"kbCoordLeft"=:kbcoordleft,"kbCoordTop"=:kbcoordtop,"focdItemCoordLeft"=:focditemcoordleft,
"focdItemCoordTop"=:focditemcoordtop,"tbCountry"=:tbcountry,"tbCompany"=:tbcompany,"tbLanguage"=:tblanguage,"tbPriority"=:tbpriority,
"tbStatus"=:tbstatus,"tbStep"=:tbstep,"tbStepStatus"=:tbstepstatus,"tbState"=:tbstate,"tbWfReasonNote"=:tbwfreasonnote,"tbStage"=:tbstage,
"tbPhase"=:tbphase,"tbPine"=:tbpine,"tbBalsam"=:tbbalsam,"tbFir"=:tbfir,"tbSpruce"=:tbspruce,"tbArea"=:tbarea,"tbSize"=:tbsize,
"tbPercent2"=:tbpercent2,"tbVolume"=:tbvolume,"tbT1"=:tbt1,"tbT2"=:tbt2,"tbT3"=:tbt3,"tbT4"=:tbt4,"tbT5"=:tbt5,"tbTaskParent"=:tbtaskparent
where
tbid=:id;
end;



/* final sql */
canvasno: 1
focditemcoordleft: 0
focditemcoordtop: 0
kbcoordleft: 0
kbcoordtop: 0
tbafinish: ""
tbarea: ""
tbastart: ""
tbawork: 0
tbazureid: "01"
tbbalsam: ""
tbbarcolor: "bbb"
tbblid: 0
tbcalendar: 8
tbcompany: "Company"
tbconstraintdate: ""
tbconstrainttype: ""
tbcoordleft: 210
tbcoordtop: 107
tbcost: 0
tbcostid: ""
tbcostremaining: 0
tbcosttype: "01"
tbcountry: ""
tbcustomerid: "44"
tbduration: 5
tbexphoursperweek: 0
tbfinish: "13-Jan-2022"
tbfir: ""
tbfloat: 0
tbfreefloat: 0
tbhierarchyorder: 1
tbid: "526"
tbl1: "My Proj"
tbl2: "My Project or Program"
tbl3: "My SubProject or Project if program above it"
tbl4: "My Task or my SubProject if program above"
tbl5: "My Allocation or task if program above"
tblanguage: "tbLanguage"
tbmetadataid: ""
tbname: "My New Portfolio"
tbowner: ""
tbpayrate: 0
tbpercent2: ""
tbpercentcomplete: 0
tbpercenttimeon: 0
tbphase: ""
tbpine: ""
tbpredecessor: ""
tbpriority: "tbPriority"
tbremainingduration: 5
tbresid: ""
tbselfkey2: "525"
tbsize: ""
tbspruce: ""
tbstage: ""
tbstart: "6-Jan-2021"
tbstate: ""
tbstatus: "tbStatus"
tbstep: ""
tbstepstatus: ""
tbt1: ""
tbt2: ""
tbt3: ""
tbt4: ""
tbt5: ""
tbtaskparent: ""
tbtextcolor: "bbb"
tbtype: "Project"
tbvolume: ""
tbwfreasonnote: ""
tbwork: 0
tbworkremaining: 0


,
"tbRemainingDuration","tbResID","tbCalendar","tbPercentTimeOn","tbWork","tbAWork","tbWorkRemaining",
"tbPercentComplete","tbCost","tbAcost","tbCostRemaining","tbPayRate","tbCostType","tbCostID",
"tbConstraintType","tbConstraintDate","tbFloat","tbFreeFloat","tbExpHoursPerWeek","canvasNo",
"tbCustomerID","tbMetaDataID","tbBLID","tbPredecessor","tbAzureID","tbBarColor",
"tbTextColor","kbCoordLeft","kbCoordTop","focdItemCoordLeft","focdItemCoordTop"


, 
:tbremainingduration, :tbresid, :tbcalendar, :tbpercenttimeon, :tbwork, :tbawork, :tbworkremaining, 
:tbpercentcomplete, :tbcost, :tbacost, :tbcostremaining, :tbpayrate, :tbcosttype, :tbcostid, 
:tbconstrainttype, :tbconstraintdate, :tbfloat, :tbfreefloat, :tbexphoursperweek, :canvasno, :tbcustomerid, 
:tbmetadataid, :tbblid, :tbpredecessor, :tbazureid, :tbbarcolor, 
:tbtextcolor, :kbcoordleft, :kbcoordtop, :focditemcoordleft, :focditemcoordtop

/* problem ros */
,"tbPercentTimeOn","tbWork","tbAWork","tbWorkRemaining"

/* could be */
,"tbResID","tbCalendar"


/* problem ,tbRemainingDuration" */

,"tbPercentComplete","tbCost","tbAcost","tbCostRemaining","tbPayRate","tbCostType","tbCostID"

,"tbConstraintType","tbConstraintDate","tbFloat","tbFreeFloat","tbExpHoursPerWeek","canvasNo",
"tbCustomerID","tbMetaDataID","tbBLID","tbPredecessor","tbAzureID","tbBarColor",

"tbTextColor","kbCoordLeft","kbCoordTop","focdItemCoordLeft","focdItemCoordTop","tbCountry","tbCompany",
"tbLanguage","tbPriority","tbStatus","tbStep","tbStepStatus","tbState","tbWfReasonNote","tbStage",

"tbPhase","tbPine","tbBalsam","tbFir","tbSpruce","tbArea","tbSize","tbPercent2","tbVolume","tbT1",
"tbT2","tbT3","tbT4","tbT5","tbTaskParent"