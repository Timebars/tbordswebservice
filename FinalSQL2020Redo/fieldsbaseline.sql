/* baseline select */
select 
"tbID","tbSelfKey2","tbCoordTop","tbCoordLeft","tbHierarchyOrder","tbName","tbType","tbL1","tbL2",
"tbL3","tbL4","tbL5","tbOwner","tbStart","tbFinish","tbDuration","tbAStart","tbAFinish","tbResID",
"tbCalendar","tbPercentTimeOn","tbWork","tbAWork","tbWorkRemaining",
"tbPercentComplete","tbCost","tbAcost","tbCostRemaining","tbPayRate","tbCostType","tbCostID",
"tbConstraintType","tbConstraintDate","tbFloat","tbFreeFloat","tbExpHoursPerWeek","canvasNo",
"tbCustomerID","tbMetaDataID","tbBLID","tbPredecessor","tbAzureID","tbBarColor",
"tbTextColor","kbCoordLeft","kbCoordTop","focdItemCoordLeft","focdItemCoordTop","tbCountry","tbCompany",
"tbLanguage","tbPriority","tbStatus","tbStep","tbStepStatus","tbState","tbWfReasonNote","tbStage",
"tbPhase","tbPine","tbBalsam","tbFir","tbSpruce","tbArea","tbSize","tbPercent2","tbVolume","tbT1",
"tbT2","tbT3","tbT4","tbT5","tbTaskParent","tbRemainingDuration"
from TBBASELINE



/* baseline post */

begin
insert into TBBASELINE("tbID","tbSelfKey2","tbCoordTop","tbCoordLeft","tbHierarchyOrder","tbName","tbType","tbL1","tbL2",
"tbL3","tbL4","tbL5","tbOwner","tbStart","tbFinish","tbDuration","tbAStart","tbAFinish","tbResID",
"tbCalendar","tbPercentTimeOn","tbWork","tbAWork","tbWorkRemaining",
"tbPercentComplete","tbCost","tbAcost","tbCostRemaining","tbPayRate","tbCostType","tbCostID",
"tbConstraintType","tbConstraintDate","tbFloat","tbFreeFloat","tbExpHoursPerWeek","canvasNo",
"tbCustomerID","tbMetaDataID","tbBLID","tbPredecessor","tbAzureID","tbBarColor",
"tbTextColor","kbCoordLeft","kbCoordTop","focdItemCoordLeft","focdItemCoordTop","tbCountry","tbCompany",
"tbLanguage","tbPriority","tbStatus","tbStep","tbStepStatus","tbState","tbWfReasonNote","tbStage",
"tbPhase","tbPine","tbBalsam","tbFir","tbSpruce","tbArea","tbSize","tbPercent2","tbVolume","tbT1",
"tbT2","tbT3","tbT4","tbT5","tbTaskParent","tbRemainingDuration")
values (
:tbid, :tbselfkey2, :tbcoordtop, :tbcoordleft, :tbhierarchyorder, :tbname, :tbtype, :tbl1, :tbl2, 
:tbl3, :tbl4, :tbl5, :tbowner, :tbstart, :tbfinish, :tbduration, :tbastart, :tbafinish, 
:tbresid, :tbcalendar, :tbpercenttimeon, :tbwork, :tbawork, :tbworkremaining, 
:tbpercentcomplete, :tbcost, :tbacost, :tbcostremaining, :tbpayrate, :tbcosttype, :tbcostid, 
:tbconstrainttype, :tbconstraintdate, :tbfloat, :tbfreefloat, :tbexphoursperweek, :canvasno, :tbcustomerid, 
:tbmetadataid, :tbblid, :tbpredecessor, :tbazureid, :tbbarcolor, 
:tbtextcolor, :kbcoordleft, :kbcoordtop, :focditemcoordleft, :focditemcoordtop, :tbcountry,
:tbcompany, :tblanguage, :tbpriority, :tbstatus, :tbstep, :tbstepstatus, :tbstate, :tbwfreasonnote, 
:tbstage, :tbphase, :tbpine, :tbbalsam, :tbfir, :tbspruce, :tbarea, :tbsize, :tbpercent2, :tbvolume, 
:tbt1, :tbt2, :tbt3, :tbt4, :tbt5, :tbtaskparent, :tbremainingduration
);
end;


/* baseline put */

begin
update TBBASELINE set 
"tbID"=:tbid,"tbSelfKey2"=:tbselfkey2,"tbCoordTop"=:tbcoordtop,"tbCoordLeft"=:tbcoordleft,"tbHierarchyOrder"=:tbhierarchyorder,
"tbName"=:tbname,"tbType"=:tbtype,"tbL1"=:tbl1,"tbL2"=:tbl2,"tbL3"=:tbl3,"tbL4"=:tbl4,"tbL5"=:tbl5,"tbOwner"=:tbowner,"tbStart"=:tbstart,
"tbFinish"=:tbfinish,"tbDuration"=:tbduration,"tbAStart"=:tbastart,"tbAFinish"=:tbafinish,"tbRemainingDuration"=:tbremainingduration,
"tbResID"=:tbresid,"tbCalendar"=:tbcalendar,"tbPercentTimeOn"=:tbpercenttimeon,"tbWork"=:tbwork,"tbAWork"=:tbawork,
"tbWorkRemaining"=:tbworkremaining,"tbPercentComplete"=:tbpercentcomplete,"tbCost"=:tbcost,"tbAcost"=:tbacost,
"tbCostRemaining"=:tbcostremaining,"tbPayRate"=:tbpayrate,"tbCostType"=:tbcosttype,"tbCostID"=:tbcostid,
"tbConstraintType"=:tbconstrainttype,"tbConstraintDate"=:tbconstraintdate,"tbFloat"=:tbfloat,"tbFreeFloat"=:tbfreefloat,
"tbExpHoursPerWeek"=:tbexphoursperweek,"canvasNo"=:canvasno,"tbCustomerID"=:tbcustomerid,"tbMetaDataID"=:tbmetadataid,
"tbBLID"=:tbblid,"tbPredecessor"=:tbpredecessor,"tbAzureID"=:tbazureid,"tbBarColor"=:tbbarcolor,
"tbTextColor"=:tbtextcolor,"kbCoordLeft"=:kbcoordleft,"kbCoordTop"=:kbcoordtop,"focdItemCoordLeft"=:focditemcoordleft,
"focdItemCoordTop"=:focditemcoordtop,"tbCountry"=:tbcountry,"tbCompany"=:tbcompany,"tbLanguage"=:tblanguage,"tbPriority"=:tbpriority,
"tbStatus"=:tbstatus,"tbStep"=:tbstep,"tbStepStatus"=:tbstepstatus,"tbState"=:tbstate,"tbWfReasonNote"=:tbwfreasonnote,"tbStage"=:tbstage,
"tbPhase"=:tbphase,"tbPine"=:tbpine,"tbBalsam"=:tbbalsam,"tbFir"=:tbfir,"tbSpruce"=:tbspruce,"tbArea"=:tbarea,"tbSize"=:tbsize,
"tbPercent2"=:tbpercent2,"tbVolume"=:tbvolume,"tbT1"=:tbt1,"tbT2"=:tbt2,"tbT3"=:tbt3,"tbT4"=:tbt4,"tbT5"=:tbt5,"tbTaskParent"=:tbtaskparent
where
"tbID"=:id;
end;



/* final sql */




  CREATE TABLE "TIMEBARS2"."TBBASELINE" 
   (	"id" NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  NOT NULL ENABLE, 
	"tbID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbL1" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbL2" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbL3" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbL4" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbL5" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbBLID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbCost" NUMBER(13,4), 
	"tbName" VARCHAR2(100 BYTE) COLLATE "USING_NLS_COMP", 
	"tbType" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbWork" NUMBER, 
	"tbAcost" NUMBER(13,4), 
	"tbAWork" NUMBER, 
	"tbFloat" NUMBER, 
	"tbOwner" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbResID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbStart" DATE, 
	"canvasNo" NUMBER, 
	"tbAStart" DATE, 
	"tbCostID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbFinish" DATE, 
	"tbAFinish" DATE, 
	"tbAzureID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbPayRate" NUMBER, 
	"kbCoordTop" NUMBER, 
	"tbCalendar" NUMBER, 
	"tbCoordTop" NUMBER, 
	"tbCostType" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDuration" NUMBER, 
	"tbSelfKey2" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"kbCoordLeft" NUMBER, 
	"tbCoordLeft" NUMBER, 
	"tbFreeFloat" NUMBER, 
	"tbCustomerID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbMetaDataID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbPredecessor" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbCostRemaining" NUMBER(13,4), 
	"tbPercentTimeOn" NUMBER, 
	"tbWorkRemaining" NUMBER, 
	"tbConstraintDate" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbConstraintType" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbExpHoursPerWeek" NUMBER, 
	"tbPercentComplete" NUMBER, 
	"tbRemainingDuration" NUMBER, 
	"tbBarColor" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbTextColor" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"focdItemCoordLeft" NUMBER, 
	"focdItemCoordTop" NUMBER, 
	"tbHierarchyOrder" NUMBER, 
	"tbCountry" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbCompany" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbLanguage" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbPriority" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbStatus" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbStep" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbStepStatus" VARCHAR2(2000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbState" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbWfReasonNote" VARCHAR2(2000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbStage" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbPhase" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbPine" NUMBER, 
	"tbBalsam" NUMBER, 
	"tbFir" NUMBER, 
	"tbSpruce" NUMBER, 
	"tbArea" NUMBER, 
	"tbSize" NUMBER, 
	"tbPercent2" NUMBER, 
	"tbVolume" NUMBER, 
	"tbT1" VARCHAR2(500 BYTE) COLLATE "USING_NLS_COMP", 
	"tbT2" VARCHAR2(500 BYTE) COLLATE "USING_NLS_COMP", 
	"tbT3" VARCHAR2(500 BYTE) COLLATE "USING_NLS_COMP", 
	"tbT4" VARCHAR2(500 BYTE) COLLATE "USING_NLS_COMP", 
	"tbT5" VARCHAR2(500 BYTE) COLLATE "USING_NLS_COMP", 
	"tbTaskParent" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"CREATED" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"UPDATED" TIMESTAMP (6), 
	"UPDATED_BY" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP"
   )  DEFAULT COLLATION "USING_NLS_COMP" SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 10 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA" ;
  CREATE UNIQUE INDEX "TIMEBARS2"."TBBASELINE_CON" ON "TIMEBARS2"."TBBASELINE" ("tbID") 
  PCTFREE 10 INITRANS 20 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA" ;

ALTER TABLE "TIMEBARS2"."TBBASELINE" ADD CONSTRAINT "TBBASELINE_CON" PRIMARY KEY ("tbID")
  USING INDEX "TIMEBARS2"."TBBASELINE_CON"  ENABLE;

  CREATE OR REPLACE EDITIONABLE TRIGGER "TIMEBARS2"."BIU_TBBASELINE" BEFORE
    INSERT OR UPDATE ON TBBASELINE
    FOR EACH ROW
BEGIN
    IF INSERTING THEN
        :NEW.CREATED := LOCALTIMESTAMP;
        :NEW.CREATED_BY := NVL(
                              WWV_FLOW.G_USER,
                              USER
                           );
    END IF;

    :NEW.UPDATED := LOCALTIMESTAMP;
    :NEW.UPDATED_BY := NVL(
                          WWV_FLOW.G_USER,
                          USER
                       );
END;
/
ALTER TRIGGER "TIMEBARS2"."BIU_TBBASELINE" ENABLE;
