
/* docs get */
"tbDocID","tbDocName","tbDocNameShort","tbDocStatus","tbDocPurpose","tbDocWrittenBy","tbDocBriefDescription",
"tbDocCustomerID","tbDocType","tbDocHTMLContent","tbDocGroup","tbDocOwner","tbDocPopular","tbDocSortOrder",
"tbDocLastModified","tbDocApprovedBy","tbDocL1","tbDocL2","tbDocL3","tbDocL4","tbDocL5","tbDocCardImage",
"tbDocSize","tbDocLink","tbDocVersion","tbDocMimeType","tbDocReferences","tbDocHolds","tbDocCategory",
"tbDocNickName","tbDocT1","tbDocT2","tbDocT3","tbDocT4","tbDocT5"

/* docs post */

:tbdocid, :tbdocname, :tbdocnameshort, :tbdocstatus, :tbdocpurpose, :tbdocwrittenby, :tbdocbriefdescription, :tbdoccustomerid, :tbdoctype, :tbdochtmlcontent, :tbdocgroup, :tbdocowner, :tbdocpopular, :tbdocsortorder, :tbdoclastmodified, :tbdocapprovedby, :tbdocl1, :tbdocl2, :tbdocl3, :tbdocl4, :tbdocl5, :tbdoccardimage, :tbdocsize, :tbdoclink, :tbdocversion, :tbdocmimetype, :tbdocreferences, :tbdocholds, :tbdoccategory, :tbdocnickname, :tbdoct1, :tbdoct2, :tbdoct3, :tbdoct4, :tbdoct5


/* docs put */

"tbDocID"=:tbdocid,"tbDocName"=:tbdocname,"tbDocNameShort"=:tbdocnameshort,"tbDocStatus"=:tbdocstatus,"tbDocPurpose"=:tbdocpurpose,"tbDocWrittenBy"=:tbdocwrittenby,"tbDocBriefDescription"=:tbdocbriefdescription,"tbDocCustomerID"=:tbdoccustomerid,"tbDocType"=:tbdoctype,"tbDocHTMLContent"=:tbdochtmlcontent,"tbDocGroup"=:tbdocgroup,"tbDocOwner"=:tbdocowner,"tbDocPopular"=:tbdocpopular,"tbDocSortOrder"=:tbdocsortorder,"tbDocLastModified"=:tbdoclastmodified,"tbDocApprovedBy"=:tbdocapprovedby,"tbDocL1"=:tbdocl1,"tbDocL2"=:tbdocl2,"tbDocL3"=:tbdocl3,"tbDocL4"=:tbdocl4,"tbDocL5"=:tbdocl5,"tbDocCardImage"=:tbdoccardimage,"tbDocSize"=:tbdocsize,"tbDocLink"=:tbdoclink,"tbDocVersion"=:tbdocversion,"tbDocMimeType"=:tbdocmimetype,"tbDocReferences"=:tbdocreferences,"tbDocHolds"=:tbdocholds,"tbDocCategory"=:tbdoccategory,"tbDocNickName"=:tbdocnickname,"tbDocT1"=:tbdoct1,"tbDocT2"=:tbdoct2,"tbDocT3"=:tbdoct3,"tbDocT4"=:tbdoct4,"tbDocT5"=:tbdoct5,

/* final sql */

SELECT "id", "Pine Vol" As "Volume", 'Pine' As "Species"
FROM FORESTRY
WHERE "Pine Pct Vol" > 0 AND "Stage" = 'HC' AND ("tbAFinish" IS NULL) AND "Type" = 'Task'
UNION ALL
SELECT "id", "Balsam Vol" As "Volume", 'Balsam' As "Species"
FROM FORESTRY
WHERE "Balsam Pct Vol" > 0 AND "Stage" = 'HC' AND ("tbAFinish" IS NULL) AND "Type" = 'Task'
UNION ALL
SELECT "id", "Fir Vol" As "Volume", 'Fir' As "Species"
FROM FORESTRY
WHERE "Fir Pct Vol" > 0 AND "Stage" = 'HC' AND ("tbAFinish" IS NULL) AND "Type" = 'Task'
UNION ALL
SELECT "id", "Spruce Vol" As "Volume", 'Spruce' As "Species"
FROM FORESTRY
WHERE "Spruce Pct Vol" > 0 AND "Stage" = 'HC' AND ("tbAFinish" IS NULL) AND "Type" = 'Task'


  CREATE TABLE "TIMEBARS2"."TBDOCUMENTS" 
   (	"id" NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  NOT NULL ENABLE, 
	"tbDocID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocL1" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocL2" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocL3" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocL4" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocL5" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocName" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocType" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocGroup" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocOwner" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocStatus" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocPopular" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocPurpose" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocCardImage" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocNameShort" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocSortOrder" NUMBER, 
	"tbDocWrittenBy" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocApprovedBy" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocCustomerID" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocHTMLContent" VARCHAR2(8000 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocLastModified" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocBriefDescription" VARCHAR2(500 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocSize" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocLink" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocVersion" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocMimeType" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocReferences" VARCHAR2(500 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocHolds" VARCHAR2(500 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocCategory" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocNickName" VARCHAR2(50 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocT1" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocT2" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocT3" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocT4" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"tbDocT5" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"CREATED" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP", 
	"UPDATED" TIMESTAMP (6), 
	"UPDATED_BY" VARCHAR2(255 BYTE) COLLATE "USING_NLS_COMP"
   )  DEFAULT COLLATION "USING_NLS_COMP" SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 10 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA" ;
  CREATE UNIQUE INDEX "TIMEBARS2"."TBDOCUMENTS_CON" ON "TIMEBARS2"."TBDOCUMENTS" ("tbDocID") 
  PCTFREE 10 INITRANS 20 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA" ;

ALTER TABLE "TIMEBARS2"."TBDOCUMENTS" ADD CONSTRAINT "TBDOCUMENTS_CON" PRIMARY KEY ("tbDocID")
  USING INDEX "TIMEBARS2"."TBDOCUMENTS_CON"  ENABLE;

  CREATE OR REPLACE EDITIONABLE TRIGGER "TIMEBARS2"."BIU_TBDOCUMENTS" 
    before insert or update on TBDOCUMENTS
    for each row
begin

    if inserting then
        :new.created    := localtimestamp;
        :new.created_by := nvl(wwv_flow.g_user,user);
    end if;
    :new.updated    := localtimestamp;
    :new.updated_by := nvl(wwv_flow.g_user,user);
end;
/
ALTER TRIGGER "TIMEBARS2"."BIU_TBDOCUMENTS" ENABLE;

