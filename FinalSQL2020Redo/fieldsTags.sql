
/* tags get */
select
"tbTagID","tbTagGroup","tbTagName","tbTagEntity","tbTagPopular","tbTagNameShort",
"tbTagPurpose","tbTagDescription","tbTagOwner","tbTagSortOrder","tbTagCustomerID",
"tbTagAzureID","tbTagLastModified","tbTagMethod","tbTagSite","tbTagL1",
"tbTagL2","tbTagL3","tbTagL4","tbTagL5"
from TBTAGS



/* tags post */

begin
insert into TBTAGS(
"tbTagID","tbTagGroup","tbTagName","tbTagEntity","tbTagPopular","tbTagNameShort",
"tbTagPurpose","tbTagDescription","tbTagOwner","tbTagSortOrder","tbTagCustomerID",
"tbTagAzureID","tbTagLastModified","tbTagMethod","tbTagSite","tbTagL1",
"tbTagL2","tbTagL3","tbTagL4","tbTagL5"
)
values (
:tbtagid, :tbtaggroup, :tbtagname, :tbtagentity, :tbtagpopular, :tbtagnameshort, 
:tbtagpurpose, :tbtagdescription, :tbtagowner, :tbtagsortorder, :tbtagcustomerid, 
:tbtagazureid, :tbtaglastmodified, :tbtagmethod, :tbtagsite, :tbtagl1, 
:tbtagl2, :tbtagl3, :tbtagl4, :tbtagl5
);
end;


/* tags put */


"tbTagID"=:tbtagid,"tbTagGroup"=:tbtaggroup,"tbTagName"=:tbtagname,"tbTagEntity"=:tbtagentity,"tbTagPopular"=:tbtagpopular,"tbTagNameShort"=:tbtagnameshort,"tbTagPurpose"=:tbtagpurpose,"tbTagDescription"=:tbtagdescription,"tbTagOwner"=:tbtagowner,"tbTagSortOrder"=:tbtagsortorder,"tbTagCustomerID"=:tbtagcustomerid,"tbTagAzureID"=:tbtagazureid,"tbTagLastModified"=:tbtaglastmodified,"tbTagMethod"=:tbtagmethod,"tbTagSite"=:tbtagsite,"tbTagL1"=:tbtagl1,"tbTagL2"=:tbtagl2,"tbTagL3"=:tbtagl3,"tbTagL4"=:tbtagl4,"tbTagL5"=:tbtagl5

/* final sql */



        "tbTagID",
        "tbTagGroup",
        "tbTagName",
        "tbTagEntity",
        "tbTagPopular",
        "tbTagNameShort",
        "tbTagPurpose",
        "tbTagDescription",
        "tbTagOwner",
        "tbTagSortOrder",
        "tbTagCustomerID",
        "tbTagAzureID",
        "tbTagLastModified",
        "tbTagMethod",
        "tbTagSite",
        "tbTagL1",
        "tbTagL2",
        "tbTagL3",
        "tbTagL4",
        "tbTagL5"