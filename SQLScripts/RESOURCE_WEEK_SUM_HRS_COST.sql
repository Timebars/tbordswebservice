SELECT
    timebars2.tbrescalcs."tbResCalcWeek"   AS "WeekNo",
    timebars2.tbresources.tbresname        AS "Name",
    SUM(timebars2.tbrescalcs."tbResCalcWeekSumHours") AS "SumHours",
    SUM(timebars2.tbrescalcs."tbResCalcWeekSumCost") AS "SumCost",
    timebars2.tbresources.tbresteam,
    timebars2.tbresources.tbressupervisor,
    timebars2.tbresources."tbResPrimarySkill",
    timebars2.tbresources."tbResPrimaryRole",
    timebars2.tbresources."tbResLabourType",
    timebars2.tbresources."tbResPartTimeFullTime",
    timebars2.tbrescalcs.tbrescalclastmodified
FROM
    timebars2.tbrescalcs
    INNER JOIN timebars2.tbresources ON timebars2.tbrescalcs."tbResCalcTbResID" = timebars2.tbresources.tbresid
WHERE
    timebars2.tbrescalcs.tbrescalcname = 'WeeklyHoursAndCost'
GROUP BY
    timebars2.tbrescalcs."tbResCalcWeek",
    timebars2.tbresources.tbresname,
    timebars2.tbresources.tbresteam,
    timebars2.tbresources.tbressupervisor,
    timebars2.tbresources."tbResPrimarySkill",
    timebars2.tbresources."tbResPrimaryRole",
    timebars2.tbresources."tbResLabourType",
    timebars2.tbresources."tbResPartTimeFullTime",
    timebars2.tbrescalcs.tbrescalclastmodified