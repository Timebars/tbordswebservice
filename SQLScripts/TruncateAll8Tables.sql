

truncate table "TIMEBARS2"."TBTIMEBARS" drop storage;

truncate table "TIMEBARS2"."TBRESOURCES" drop storage;

truncate table "TIMEBARS2"."TBADMINPANEL" drop storage;

truncate table "TIMEBARS2"."TBRESCALCS" drop storage;

truncate table "TIMEBARS2"."TBTAGS" drop storage;

truncate table "TIMEBARS2"."TBBASELINE" drop storage;

truncate table "TIMEBARS2"."TBMETADATA" drop storage;

truncate table "TIMEBARS2"."TBDOCUMENTS" drop storage;
