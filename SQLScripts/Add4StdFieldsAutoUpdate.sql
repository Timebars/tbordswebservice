 
 -- add these 4 fields first
"CREATED" TIMESTAMP(6) ,
"CREATED_BY" VARCHAR2(255 BYTE),
"UPDATED" TIMESTAMP(6), 
"UPDATED_BY" VARCHAR2(255 BYTE)  
 
 -- then run this to auto populate them all
  CREATE OR REPLACE EDITIONABLE TRIGGER "TIMEBARS2"."BIU_xxx" 
    before insert or update on xxx
    for each row
begin

    if inserting then
        :new.created    := localtimestamp;
        :new.created_by := nvl(wwv_flow.g_user,user);
    end if;
    :new.updated    := localtimestamp;
    :new.updated_by := nvl(wwv_flow.g_user,user);
end;

/
ALTER TRIGGER "TIMEBARS2"."BIU_xxx" ENABLE;


