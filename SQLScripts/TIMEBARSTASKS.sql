SELECT
    timebars2.tbtimebars."tbID",
    timebars2.tbtimebars."tbSelfKey2",
    timebars2.tbtimebars."tbCoordTop",
    timebars2.tbtimebars."tbHierarchyOrder",
    timebars2.tbtimebars."tbName",
    timebars2.tbtimebars."tbType",
    timebars2.tbtimebars."tbL1",
    timebars2.tbtimebars."tbL2",
    timebars2.tbtimebars."tbL3",
    timebars2.tbtimebars."tbStart",
    timebars2.tbtimebars."tbFinish",
    timebars2.tbtimebars."tbDuration",
    timebars2.tbtimebars."tbRemainingDuration",
    timebars2.tbtimebars."tbOwner",
    timebars2.tbtimebars."tbAStart",
    timebars2.tbtimebars."tbAFinish",
    timebars2.tbtimebars."tbWork",
    timebars2.tbtimebars."tbAWork",
    timebars2.tbtimebars."tbWorkRemaining",
    timebars2.tbtimebars."tbPercentComplete",
    timebars2.tbtimebars."tbConstraintType",
    timebars2.tbtimebars."tbCost",
    timebars2.tbtimebars."tbAcost",
    timebars2.tbtimebars."tbCostRemaining",
    timebars2.tbtimebars."tbPayRate",
    timebars2.tbtimebars."tbCalendar",
    timebars2.tbtimebars."tbCostType",
    timebars2.tbtimebars."tbPredecessor",
    timebars2.tbtimebars."tbTaskParent",
    timebars2.tbtimebars."tbPriority",
    timebars2.tbtimebars."tbStatus",
    timebars2.tbtimebars."tbStep",
    timebars2.tbtimebars."tbStepStatus",
    timebars2.tbtimebars."tbState",
    timebars2.tbtimebars."tbWfReasonNote",
    timebars2.tbtimebars."tbStage",
    timebars2.tbtimebars."tbPhase",
    timebars2.tbtimebars."tbExpHoursPerWeek",
    timebars2.tbtimebars."tbPercentTimeOn",
    timebars2.tbmetadata."tbMDPM",
    timebars2.tbmetadata."tbMDNotes",
    timebars2.tbmetadata."tbMDResponsibility",
    timebars2.tbmetadata."tbMDExecutiveSummary",
    timebars2.tbmetadata."tbMDDescription"
FROM
    timebars2.tbtimebars
    INNER JOIN timebars2.tbmetadata ON timebars2.tbtimebars."tbID" = timebars2.tbmetadata."tbMDID"
WHERE
    timebars2.tbtimebars."tbType" = 'Task'