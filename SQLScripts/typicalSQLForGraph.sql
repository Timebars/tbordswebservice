

--did not work
select
'TimebarsRescalcs' as label,
sum(case when tbResCalcWeekSumHours >= 1000 then tbResCalcWeekSumHours else 0 end ) as Value
from TBRESCALCS 
where TBRESCALCNAME = 'WeeklyHoursAndCost'
union all (
select
'TimebarsRescalcs2' as label2,
sum(case when tbResCalcWeekSumHours < 1000 then tbResCalcWeekSumHours else 0 end ) as Value2
from TBRESCALCS 
where TBRESCALCNAME = 'WeeklyHoursAndCost'
)


-- did work with all small letters and quotes around  tbResCalcWeekSumHours



select
'TimebarsRescalcs' as label,
sum(case when timebars2.tbrescalcs."tbResCalcWeekSumHours" >= 1000 then timebars2.tbrescalcs."tbResCalcWeekSumHours" else 0 end) as Value
from timebars2.tbrescalcs 
where timebars2.tbrescalcs.tbrescalcname = 'WeeklyHoursAndCost'
union all (
select
'TimebarsRescalcs2' as label2,
sum(case when timebars2.tbrescalcs."tbResCalcWeekSumHours" < 1000 then timebars2.tbrescalcs."tbResCalcWeekSumHours" else 0 end) as Value2
from timebars2.tbrescalcs
where timebars2.tbrescalcs.tbrescalcname = 'WeeklyHoursAndCost'
)

-- this is all i need for simple bar chart with res name
SELECT
    SUM(timebars2.tbrescalcs."tbResCalcWeekSumHours") AS "SumHours",
    timebars2.tbrescalcs."tbResCalcStartWeekNo" AS "WeekNo",
    timebars2.tbresources.tbresname AS "Name"
FROM
    timebars2.tbrescalcs
    INNER JOIN timebars2.tbresources ON timebars2.tbrescalcs."tbResCalcTbResID" = timebars2.tbresources.tbresid
WHERE
    timebars2.tbrescalcs.tbrescalcname = 'WeeklyHoursAndCost'
GROUP BY
    timebars2.tbrescalcs."tbResCalcStartWeekNo",
    timebars2.tbresources.tbresname