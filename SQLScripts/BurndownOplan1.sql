select
wkending,statusdate,wkhrs,cumhrs,

(cumhrs + totalhrs) AS bdhrs

FROM (

SELECT
wkending,statusdate,wkhrs,
SUM(wkhrs) OVER()  AS totalhrs,
-1 *ABS(SUM(wkhrs) OVER(ORDER BY wkending)) AS cumhrs

FROM
    (

        SELECT
            rc."tbResCalcWeek"           AS wkending,
          rc."tbResCalcLastModified" AS statusdate,  
            ROUND(SUM(rc."tbResCalcWeekSumHours"),2)   AS wkhrs
             
        FROM
            timebars2.TBRESCALCS rc
        WHERE
            rc."tbResCalcName" = 'Planned'
        GROUP BY
            rc."tbResCalcWeek", rc."tbResCalcLastModified"
        ORDER BY
            rc."tbResCalcWeek"


    ))
ORDER BY
    wkending