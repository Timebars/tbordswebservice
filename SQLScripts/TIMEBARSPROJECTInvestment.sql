
/* investment reports are for project level only for now 
or timebars2.tbtimebars."tbType" = 'Sub-Project'
Idea is to do join to TIMEBARSCORE for cost and sched info
*/


SELECT
    timebars2.tbtimebars ."tbID" ,
    timebars2 .tbtimebars."tbSelfKey2",
    timebars2 .tbtimebars ."tbHierarchyOrder" ,
    timebars2.tbtimebars."tbName" ,
    timebars2 .tbtimebars ."tbType",
    timebars2.tbmetadata ."tbMDPM" ,
    timebars2 .tbmetadata."tbMDNotes",
    timebars2 .tbmetadata ."tbMDResponsibility" ,
    timebars2.tbmetadata."tbMDExecutiveSummary" ,
    timebars2 .tbmetadata ."tbMDDescription",
    timebars2.tbmetadata ."tbMDWBS" ,
    timebars2 .tbmetadata."tbMDGate",
    timebars2 .tbmetadata ."tbMDSize" ,
    timebars2.tbmetadata."tbMDPhase" ,
    timebars2 .tbmetadata ."tbMDStage",
    timebars2.tbmetadata ."tbMDState" ,
    timebars2 .tbmetadata."tbMDShowIn",
    timebars2 .tbmetadata ."tbMDStatus" ,
    timebars2.tbmetadata."tbMDContact" ,
    timebars2 .tbmetadata ."tbMDProduct",
    timebars2.tbmetadata ."tbMDProgram" ,
    timebars2 .tbmetadata."tbMDCategory",
    timebars2 .tbmetadata ."tbMDPriority" ,
    timebars2.tbmetadata."tbMDExSponsor" ,
    timebars2 .tbmetadata ."tbMDNameShort",
    timebars2.tbmetadata ."tbMDSortOrder" ,
    timebars2 .tbmetadata."tbMDSunkCosts",
    timebars2 .tbmetadata ."tbMDWeighting" ,
    timebars2.tbmetadata."tbMDWrittenBy" ,
    timebars2 .tbmetadata ."tbMDDepartment",
    timebars2.tbmetadata ."tbMDOrgManager" ,
    timebars2 .tbmetadata."tbMDROMEstimate",
    timebars2 .tbmetadata ."tbMDNotesProject" ,
    timebars2.tbmetadata."tbMDBusinessOwner" ,
    timebars2 .tbmetadata ."tbMDNotesWorkflow",
    timebars2.tbmetadata ."tbMDPaybackPeriod" ,
    timebars2 .tbmetadata."tbMDProjectNumber",
    timebars2 .tbmetadata ."tbMDYesNoSelector" ,
    timebars2.tbmetadata."tbMDtbLastModified" ,
    timebars2 .tbmetadata ."tbMDBusinessAdvisor",
    timebars2.tbmetadata ."tbMDDeliveryManager" ,
    timebars2 .tbmetadata."tbMDEstimationClass",
    timebars2 .tbmetadata ."tbMDNetPresentValue" ,
    timebars2.tbmetadata."tbMDOpportunityCost" ,
    timebars2 .tbmetadata ."tbMDBenefitCostRatio",
    timebars2.tbmetadata ."tbMDPriorityStrategic" ,
    timebars2 .tbmetadata."tbMDInvestmentCategory",
    timebars2 .tbmetadata."tbMDInvestmentStrategy" ,
    timebars2 .tbmetadata."tbMDCostBenefitAnalysis",
    timebars2 .tbmetadata ."tbMDInvestmentObjective" ,
    timebars2.tbmetadata."tbMDInternalRateOfReturn" ,
    timebars2 .tbmetadata ."tbMDInvestmentInitiative",
    timebars2.tbmetadata ."tbMDSponsoringDepartment" ,
    timebars2 .tbmetadata."tbMDPrimaryLineOfBusiness",
    timebars2 .tbmetadata ."tbMDProgActivityAlignment" ,
    timebars2.tbmetadata."tbMDEcnomicValueAdded" ,
    timebars2 .tbmetadata ."tbMDStakeholderDescription",
    timebars2.tbmetadata ."tbMDRiskVsSizeAndComplexity"
    FROM
    timebars2.tbtimebars
    INNER JOIN timebars2.tbmetadata ON timebars2.tbtimebars."tbID" = timebars2.tbmetadata."tbMDID"
WHERE
    timebars2.tbtimebars."tbType" = 'Project' 