SELECT
    timebars2.tbrescalcs."tbResCalcID",
    timebars2.tbrescalcs."tbResCalcName",
    timebars2.tbrescalcs."tbResCalcTbResID",
    timebars2.tbrescalcs."tbResCalcWeek"           AS "WkEndng",
    timebars2.tbrescalcs."tbResCalcWeekSumHours"   AS "WeekSumHours",
    timebars2.tbrescalcs."tbResCalcWeekSumCost"    AS "WeekSumCost",

    SUM(timebars2.tbrescalcs."tbResCalcWeekSumHours") OVER(
    ORDER BY
        timebars2.tbrescalcs.id
    ) AS hourscum,
    SUM(timebars2.tbrescalcs."tbResCalcWeekSumCost") OVER(
    ORDER BY
        timebars2.tbrescalcs.id
    ) AS costcum,
    AVG(timebars2.tbrescalcs."tbResCalcWeekSumHours") OVER(
        ORDER BY
            timebars2.tbrescalcs.id
    ) AS hoursrunningavg,
    AVG(timebars2.tbrescalcs."tbResCalcWeekSumCost") OVER(
        ORDER BY
            timebars2.tbrescalcs.id
    ) AS costrunningavg,
    timebars2.tbrescalcs."tbResCalcLastModified"     AS "StatusDate"
FROM
    timebars2.tbrescalcs
WHERE
    timebars2.tbrescalcs."tbResCalcName" = 'WeeklyHoursAndCost'